package com.example.gc;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import java.io.IOException;
import java.util.Map;

public class MainVerticle extends AbstractVerticle {

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    Router router = Router.router(vertx);
    router.post("/api/grammar-check")
      .consumes("application/json")
      .handler(this::checkErrors);

    vertx.createHttpServer()
      .requestHandler(router)
      .listen(8085, http -> {
        if (http.succeeded()) {
          startPromise.complete();
          System.out.println("HTTP server started on port 8085");
        } else {
          startPromise.fail(http.cause());
        }
      });
  }

  private void checkErrors(RoutingContext routingContext) {
    routingContext.request().bodyHandler(bodyHandler -> {
      JsonObject body = bodyHandler.toJsonObject();
      String sentence = body.getString("sentence");
      vertx.executeBlocking(future -> {
        try {
          future.complete(LanguageToolCheckerService.INSTANCE.checkGrammar(sentence));
        } catch (IOException e) {
          future.complete(Map.of("error", e.getMessage()));
        }
      }, asyncResult -> {
        routingContext.response()
          .putHeader("content-type", "application/json")
          .setStatusCode(200)
          .end(Json.encodePrettily(asyncResult.result()));
      });
    });
  }

  public static void main(String[] args) {
    Vertx vertx = Vertx.vertx();
    vertx.deployVerticle(new MainVerticle());
  }
}
