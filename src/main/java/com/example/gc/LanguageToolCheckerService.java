package com.example.gc;

import com.example.gc.dto.GrammarError;
import org.languagetool.JLanguageTool;
import org.languagetool.Language;
import org.languagetool.language.BritishEnglish;
import org.languagetool.rules.RuleMatch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LanguageToolCheckerService {

  public static final LanguageToolCheckerService INSTANCE = new LanguageToolCheckerService();
  private final Language language = new BritishEnglish();

  private LanguageToolCheckerService() {}

  public List<GrammarError> checkGrammar(String sentence) throws IOException {
    JLanguageTool langTool = new JLanguageTool(language);
    List<RuleMatch> matches = langTool.check(sentence);
    List<GrammarError> errors = new ArrayList<>();
    for (RuleMatch match : matches) {
      errors.add(GrammarError.builder()
        .startLocation(match.getFromPos())
        .endLocation(match.getToPos())
        .message(match.getMessage())
        .suggestions(match.getSuggestedReplacements())
        .build());
    }
    return errors;
  }

  public static void main(String[] args) throws IOException {
    LanguageToolCheckerService languageToolCheckerService = new LanguageToolCheckerService();
    languageToolCheckerService.checkGrammar("A sentence with a error in the Hitchhiker's Guide tot he Galaxy")
      .forEach(System.out::println);
  }
}
