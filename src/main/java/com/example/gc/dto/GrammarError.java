package com.example.gc.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class GrammarError {
  private int startLocation;
  private int endLocation;
  private String message;
  private List<String> suggestions;
}
